
# Service oriented architecture

  

SOA is basically a design pattern, which is used to make any system distributed and loose coupled. with this the system becomes easy to maitain and scale. as the services are independent of one-another one doesn't effect the others, if one is having issues then other services can execute without any problem.

  
  

## SOA in development

  

We have different services which we provide to our costomers, so instead of making all of services by ourself, what we can do is we can use already made services like payment gateway and integrate it in out application that way we don't have to worry about how it works and if it starts facing issues then we can easily replace it. 

Without SOA the service is end product of application development but with SOA the application consist of already made services which are provided as application components.

Here services are refereed as different components in out application like login system, payment system, order processing...etc. 

Also instead of making services tightly coupled(merging many services together), we should make our services as independent as possible that way one service does't effect other.

  
  

## Components of SOA

1. **Application frontend**
2. **Service**
		* Contract
		* implementation
		* Interface 
		* Business logic
		* Data
4. **Service repository**
5. **Service hub**(only used by large scale enterprises)
  

## Properties of SOA

  

1. **Standardized service contract** - Service related documents, a service should have a documentation where it describes what this documentation is all about.

2. **Loose coupling** - Services should be self contained, services should be less dependent as possible, so if we change any functionality in our service then the client application won't be affected.

3. **Abstraction** - Service hide the logic from out side world.

4. **Reusability** - Serviced should be notstateful as possible. so it can be reusable and we don't have to write same code again and again.

5. **Autonomy** - Services should have complete control over underlying logic it performs. it should know about all the functionality it performs.

6. **Discoverability** - Services should me discoverable by service repository.

7. **Composability** - Services should break big problems into small ones, one should never make a service do all of the tasks, instead we should make modules.

## Advantages of SOA

1. **Service reusability** -  As services are reusable we don't have to write same code again and again.
2. **Easy maintanance** - As services are independent, maintaining them becomes easy.
3. **platform independent** - It allows us to make application composite of different sources, independent of platform.
4. **Availability** - This services are easily available to anyone.
5. **Reliability** - Application built with SOA are reliable because of independent behavior of services.
6. **Scalability** - Services can run on different servers on different location thus easily scalable.

**Disadvantages of SOA**

1. **High overhead** - validation of input parameters is done every time we access a service, so increasing response time.
2. **High investment** - High investment is required to transform out application to SOA one.
3. **Complex service management** - With large scale application number of the requests can be large, so it becomes tedious to manage this requests.

## Summery ## 

As we discussed it's good option to transform to SOA if we don't have constrains like investment and processing power, it will make our application platform independent and scalable.

## References ##

[https://www.geeksforgeeks.org/service-oriented-architecture/](https://www.geeksforgeeks.org/service-oriented-architecture/)
[https://www.guru99.com/soa-principles.html](https://www.guru99.com/soa-principles.html)
[https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec](https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec)